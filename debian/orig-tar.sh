#!/bin/sh -e

VERSION=$2
TAR=../libpam4j_$VERSION.orig.tar.gz
DIR=libpam4j-$VERSION
TAG=$(echo "libpam4j-$VERSION" | sed -re's/~(alpha|beta)/-\1-/')

svn export https://svn.java.net/svn/libpam4j~svn/tags/${TAG}/ $DIR
GZIP=--best tar -c -z -f $TAR --exclude '*.jar' --exclude '*.class' $DIR
rm -rf $DIR ../$TAG

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir && echo "moved $TAR to $origDir"
fi
